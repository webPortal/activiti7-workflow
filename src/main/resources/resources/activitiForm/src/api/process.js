/**
 * @description 流程管理列表相关接口
 * @author xzp
 */

import request from '@/utils/request'
import qs from 'qs'

/********************************** 流程部署 **************************************/
/**
 * 获取流程部署列表
 *
 * @export
 * @return {*}
 */
export function getDefinitions(data) {
  return request({
    url: 'processDefinition/getDefinitions',
    method: 'GET',
    params: data
  })
}

/**
 * 启动实例
 *
 * @export
 * @return {*}
 */
export function startProcess(data) {
  return request({
      url: 'processInstance/startProcess',
      method: 'GET',
      params: data
  })
}

/**
 * 删除部署
 *
 * @export
 * @return {*}
 */
export function delDefinition(data) {
  return request({
      url: 'processDefinition/delDefinition',
      method: 'GET',
      params: data
  })
}

/********************************** 流程实例 **************************************/
/**
 * 获取流程实例列表
 *
 * @export
 * @return {*}
 */
export function getInstances(data) {
  return request({
    url: 'processInstance/getInstances',
    method: 'GET',
    params: data
  })
}

/**
 * 挂起流程实例
 *
 * @export
 * @return {*}
 */
export function suspendInstance(data) {
  return request({
    url: 'processInstance/suspendInstance',
    method: 'GET',
    params: data
  })
}

/**
 * 激活流程实例
 *
 * @export
 * @return {*}
 */
export function resumeInstance(data) {
  return request({
    url: 'processInstance/resumeInstance',
    method: 'GET',
    params: data
  })
}

/**
 * 删除流程实例
 *
 * @export
 * @return {*}
 */
export function deleteInstance(data) {
  return request({
    url: 'processInstance/deleteInstance',
    method: 'GET',
    params: data
  })
}

/********************************** 待办任务 **************************************/
/**
 * 待办任务列表
 *
 * @export
 * @return {*}
 */
export function getTasks(data) {
  return request({
    url: 'task/getTasks',
    method: 'GET',
    params: data
  })
}

/********************************** 历史任务 **************************************/
/**
 * 历史任务列表
 *
 * @export
 * @return {*}
 */
export function getInstancesByUserName(data) {
  return request({
    url: 'activitiHistory/getInstancesByUserName',
    method: 'GET',
    params: data
  })
}

/********************************** 动态表单 **************************************/

/**
 * 动态表单渲染添加
 *
 * @export
 * @return {*}
 */
export function formDefAdd(data) {
  return request({
    url: 'formDef/add',
    method: 'POST',
    data: data,
    headers: {'Content-Type': 'application/json'}
  })
}

/**
 * bpmn-动态表单渲染
 *
 * @export
 * @return {*}
 */
export function formDefQuery(data) {
  return request({
    url: 'formDef/show',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 待办-动态表单渲染
 *
 * @export
 * @return {*}
 */
export function formDataShow(data) {
  return request({
    url: '/BusHoliday/formDataShow',
    method: 'GET',
    params: data
  })
}

/**
 * 待办-动态表单数据保存
 *
 * @export
 * @return {*}
 */
export function formDataSave(data) {
  return request({
    url: '/BusHoliday/formDataSave',
    method: 'POST',
    params: {
      instTaskID: data.instTaskID,
      businessKey: data.businessKey
    },
    data: data.data
  })
}

/**
 * 我的请假列表
 *
 * @export
 * @return {*}
 */
export function getHolidayList(data) {
  return request({
    url: '/MyHoliday/getHolidayList',
    method: 'GET',
    params: data
  })
}

/**
 * 我的请假-待办数量
 *
 * @export
 * @return {*}
 */
export function getTaskNumber() {
  return request({
    url: '/BusHoliday/getTaskNumber',
    method: 'POST'
  })
}

/**
 * 我的请假列表-待办
 *
 * @export
 * @return {*}
 */
export function getTaskList(data) {
  return request({
    url: '/BusHoliday/getTaskList',
    method: 'GET',
    params: data
  })
}

/**
 * 我的请假列表-进行中
 *
 * @export
 * @return {*}
 */
export function getOngoingList(data) {
  return request({
    url: '/BusHoliday/getOngoingList',
    method: 'POST',
    params: data
  })
}

/**
 * 我的请假列表-历史
 *
 * @export
 * @return {*}
 */
export function getHistoryList(data) {
  return request({
    url: '/BusHoliday/getHistoryList',
    method: 'GET',
    params: data
  })
}

/**
 * 我的请假列表-历史流程
 *
 * @export
 * @return {*}
 */
export function getHistory(data) {
  return request({
    url: '/HistoryProcess/getHistory',
    method: 'GET',
    params: data
  })
}

/**
 * 获取审核人
 *
 * @export
 * @return {*}
 */
export function getReviewer() {
  return request({
    url: '/BusHoliday/getReviewer',
    method: 'GET'
  })
}

/**
 * 查看历史记录
 *
 * @export
 * @return {*}
 */
export function getHistoryProcessData(data) {
  return request({
    url: '/BusHoliday/getHistoryProcessData',
    method: 'GET',
    params: data
  })
}