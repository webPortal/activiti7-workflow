import 'babel-polyfill'
import './utils/debug-console'
import Vue from 'vue'
import axios from "axios";
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import * as echarts from 'echarts'
import './utils/directive'
import './icons'
import '@/assets/styles/index.scss' // 全局样式
Vue.prototype.$echarts = echarts
Vue.prototype.$bus = new Vue()

import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss'
import '@/iconfont/iconfont.css'

import {loadExtension} from '@/extension/extension-loader'
import customTitle from '@/components/globals/custom-title.vue'
import pagination from '@/components/globals/pagination.vue'
Vue.component('customTitle', customTitle)
Vue.component('pagination', pagination)

loadExtension()

Vue.use(ElementUI, { size: 'small' })

if (typeof window !== 'undefined') {
  window.axios = axios
}

Vue.config.productionTip = false

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App),
})
